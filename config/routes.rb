Rails.application.routes.draw do
  resources :comments
  resources :articles
  post 'article/:id/comments', to: 'comments#create', as: :comment_article
  get 'articles/comment/:id', to: 'comments#edit', as: :edit_comment_article
  patch 'articles/:id/like', to: 'articles#like', as: :like_article
  patch 'articles/:id/dislike', to: 'articles#dislike', as: :dislike_article
  
    #Rotas de SessionsController
    
    get 'login', to: 'seessions#new', as: :login
    post 'login', to: 'seessions#create'
    delete 'logout', to: 'seessions#destroy', as: :logout


    #ROTAS DE USERSCONTROLLERS
    get 'users', to: 'users#index', as: :users
    get 'users/new', to: 'users#new', as: :users_new
    post 'users/new', to: 'users#create'
    get 'users/:id', to: 'users#show', as: :user
    get 'users/edit/:id', to: 'users#edit', as: :users_edit
    patch 'users/edit/:id', to: 'users#update'
    delete 'user/delete/:id', to: 'users#destroy', as: :user_destroy
end
