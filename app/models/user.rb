class User < ApplicationRecord
  mount_uploader :profile_photo, PictureUploader 
  
  has_secure_password
  
  has_many :articles
  has_many :comments
  has_many :like_articles
  has_many :dislkike_articles
  
end
